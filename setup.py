#!/usr/bin/env python3

from setuptools import setup

setup(
    name='opendnd',
    version='0.0.1',
    description='Open Source online D&D session web application',
    packages=['opendnd'],
    entry_points={'console_scripts': ['opendnd=opendnd.__main__:main']},
)
