#!/usr/bin/env python3

from flask_sqlalchemy import SQLAlchemy

from . import app, db

#equipment, OtherProficiencies, Personality
sheet_features = db.Table(
    'sheet_features',
    db.Column('sheet_id', db.Integer, db.ForeignKey('sheet.id')),
    db.Column('feature_id', db.Integer, db.ForeignKey('feature.id')))

sheet_other = db.Table(
    'sheet_other', db.Column('sheet_id', db.Integer,
                             db.ForeignKey('sheet.id')),
    db.Column('other_proficiencies_id', db.Integer,
              db.ForeignKey('other_proficiencies.id')))

sheet_equipment = db.Table(
    'sheet_equipment',
    db.Column('sheet_id', db.Integer, db.ForeignKey('sheet.id')),
    db.Column('equipment_id', db.Integer, db.ForeignKey('equipment.id')))

sheet_personaility = db.Table(
    'sheet_personality',
    db.Column('sheet_id', db.Integer, db.ForeignKey('sheet.id')),
    db.Column('personality_id', db.Integer, db.ForeignKey('personality.id')))

sheet_attacks = db.Table(
    'sheet_attacks',
    db.Column('sheet_id', db.Integer, db.ForeignKey('sheet.id')),
    db.Column('attacks_id', db.Integer, db.ForeignKey('attacks.id')))


class Sheet(db.Model):
    __tablename__ = 'sheet'
    id = db.Column(db.Integer, primary_key=True)

    # Basic information
    name = db.Column(db.String(100), nullable=False)
    class_ = db.Column(db.String(100), nullable=False)
    level = db.Column(db.String(100), nullable=False)
    background = db.Column(db.String(100), nullable=False)
    race = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(100), unique=True, nullable=False)
    # When users are added
    #user = db.Column(db.Integer, db.ForeignKey('user.id'))
    alignment = db.Column(db.String(100), nullable=False)
    experience = db.Column(db.String(100), nullable=False)

    # Stats
    strength = db.Column(db.String(100), nullable=False)
    dexterity = db.Column(db.String(100), nullable=False)
    constitution = db.Column(db.String(100), nullable=False)
    intelligence = db.Column(db.String(100), nullable=False)
    wisdom = db.Column(db.String(100), nullable=False)
    charisma = db.Column(db.String(100), nullable=False)
    inspiration = db.Column(db.String(100), nullable=False)
    proficiency_bonus = db.Column(db.String(100), nullable=False)
    armor_class = db.Column(db.String(100), nullable=False)
    initiative = db.Column(db.String(100), nullable=False)
    speed = db.Column(db.String(100), nullable=False)
    current_hp = db.Column(db.String(100), nullable=False)
    temp_hp = db.Column(db.String(100), nullable=False)
    hit_dice = db.Column(db.String(100), nullable=False)

    # Death saves
    succeeded_death_saves = db.Column(db.String(100),
                                      unique=False,
                                      nullable=False)
    failed_death_saves = db.Column(db.String(100),
                                   unique=False,
                                   nullable=False)
    # Saving throws
    strength_save = db.Column(db.String(100), nullable=False)
    dexterity_save = db.Column(db.String(100), nullable=False)
    constitution_save = db.Column(db.String(100), nullable=False)
    intelligence_save = db.Column(db.String(100), nullable=False)
    wisdom_save = db.Column(db.String(100), nullable=False)
    charisma_save = db.Column(db.String(100), nullable=False)

    # Skills
    acrobatics = db.Column(db.String(100), nullable=False)
    animal_handling = db.Column(db.String(100), nullable=False)
    arcana = db.Column(db.String(100), nullable=False)
    athletics = db.Column(db.String(100), nullable=False)
    deception = db.Column(db.String(100), nullable=False)
    history = db.Column(db.String(100), nullable=False)
    insight = db.Column(db.String(100), nullable=False)
    intimidation = db.Column(db.String(100), nullable=False)
    investigation = db.Column(db.String(100), nullable=False)
    medicine = db.Column(db.String(100), nullable=False)
    nature = db.Column(db.String(100), nullable=False)
    perception = db.Column(db.String(100), nullable=False)
    performance = db.Column(db.String(100), nullable=False)
    persuasion = db.Column(db.String(100), nullable=False)
    religion = db.Column(db.String(100), nullable=False)
    slight_of_hand = db.Column(db.String(100), nullable=False)
    stealth = db.Column(db.String(100), nullable=False)
    survival = db.Column(db.String(100), nullable=False)

    # Proficiencies
    other_proficiencies = db.relationship('OtherProficiencies',
                                          secondary=sheet_other,
                                          backref=db.backref(
                                              'other_proficiencies',
                                              lazy='subquery'))
    #Attacks & Spellcasting
    attacks = db.relationship('Attacks',
                              secondary=sheet_attacks,
                              backref=db.backref('attacks', lazy='subquery'))
    #Equipment
    equipment = db.relationship('Equipment',
                                secondary=sheet_equipment,
                                backref=db.backref('equipment',
                                                   lazy='subquery'))

    # Money
    cp = db.Column(db.Integer, nullable=False, default=0)
    sp = db.Column(db.Integer, nullable=False, default=0)
    lp = db.Column(db.Integer, nullable=False, default=0)
    gp = db.Column(db.Integer, nullable=False, default=0)
    pp = db.Column(db.Integer, nullable=False, default=0)

    #Personality Traits
    personality = db.relationship('Personality',
                                  secondary=sheet_personaility,
                                  backref=db.backref('personality',
                                                     lazy='subquery'))
    #Ideals
    ideals = db.Column(db.String(1000), nullable=False)
    #Bonds
    bonds = db.Column(db.String(1000), nullable=False)
    #Flaws
    flaws = db.Column(db.String(1000), nullable=False)
    #Features and Traits
    features = db.relationship(
        'Feature',
        secondary=sheet_features,
        backref=db.backref('feature', lazy='subquery'),
    )

    def merge_json(self, json):
        """Merge self with json"""
        for attr, value in json.items():
            if attr not in self.__mapper__.relationships.keys():
                setattr(self, attr, value)
            else:
                # Oh boy here we go
                print(attr)

    def __repr__(self):
        return '<User: %r' % self.username


class Attacks(db.Model):
    __tablename__ = 'attacks'
    id = db.Column(db.Integer, primary_key=True)
    item = db.Column(db.String(1000), nullable=False)
    attack = db.Column(db.String(1000), nullable=False)
    damage = db.Column(db.String(1000), nullable=False)


class Equipment(db.Model):
    __tablename__ = 'equipment'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(10000), nullable=False)


class OtherProficiencies(db.Model):
    __tablename__ = 'other_proficiencies'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(10000), nullable=False)


class Personality(db.Model):
    __tablename__ = 'personality'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(10000), nullable=False)


class Feature(db.Model):
    __tablename__ = 'feature'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(10000), nullable=False)


class Map(db.Model):
    __tablename__ = 'map'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    # When users are added
    #user = db.Column(db.Integer, db.ForeignKey('user.id'))
    map_data = db.Column(db.Text, nullable=False)


class Session(db.Model):
    #Defines D&D session, requires ID, name, members ID,
    #Character sheet id, and map id
    __tablename__ = 'session'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=False, nullable=False)
    members = db.Column(db.String, unique=False, nullable=False)
    characters = db.Column(db.String, unique=False, nullable=False)
    map_ID = db.Column(db.Integer, unique=False, nullable=False)

class Account(db.Model):
    __tablename__='account'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=False, nullable=False)
    password = db.Column(db.String, unique=False, nullable=False)
    sessions = db.Column(db.String, unique=False, nullable=True)
    characters = db.Column(db.String, unique=False, nullable=True)
    dateOfBirth = db.Column(db.String, unique=False, nullable=False)
