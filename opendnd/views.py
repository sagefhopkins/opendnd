#!/usr/bin/python3
import json

from flask import render_template, request, jsonify, make_response, session
from flask_socketio import emit
import datetime
from sqlalchemy.orm import scoped_session, sessionmaker
from passlib.hash import sha256_crypt

from . import app, db, socketio

from .models import *


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/character/<int:sheet_id>', methods=["POST", "GET"])
def character(sheet_id):
    sheet = Sheet.query.get(sheet_id)
    return render_template('character.html', sheet=sheet)


@app.route('/character/edit/<int:sheet_id>/json', methods=["POST"])
def character_edit_json(sheet_id):
    sheet = Sheet.query.get_or_404(sheet_id)
    if request.method == 'POST':
        json = request.get_json()
        if json['type'] == 'fet':
            sheet.features.append(Feature(content=json['content']))
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'equ':
            sheet.equipment.append(Equipment(content=json['content']))
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'pst':
            sheet.personality.append(Personality(content=json['content']))
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'bon':
            sheet.bonds = json['content']
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'fla':
            sheet.flaws = json['content']
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'ide':
            sheet.ideals = json['content']
            db.session.add(sheet)
            db.session.commit()
            return make_response(jsonify({"content": json['content']}))
        if json['type'] == 'asc':
            new = Attacks(item=json['item'],
                          attack=json['attack'],
                          damage=json['damage'])
            sheet.attacks.append(new)
            db.session.add(sheet)
            db.session.commit()
            return make_response(
                jsonify({
                    "item": json['item'],
                    "attack": json['attack'],
                    "damage": json['damage']
                }))


@app.route('/character/edit/<int:sheet_id>', methods=["POST", "GET"])
def character_edit(sheet_id):
    sheet = Sheet.query.get_or_404(sheet_id)
    if request.method == "POST":
        for attr in request.form:
            setattr(sheet, attr, request.form[attr])

        db.session.add(sheet)
        db.session.commit()

    return render_template('update.html', sheet=sheet)


@socketio.on('move_object')
def move_object(message):
    emit('new_object', message, broadcast=True, include_self=False)


@app.route('/map/<string:map_name>', methods=["POST", "GET"])
def map(map_name):
    current_map = Map.query.filter_by(name=map_name).first()
    if not current_map:
        current_map = Map(name=map_name)

    if request.method == 'POST':
        try:
            map_data = request.get_json()
            current_map.map_data = json.dumps(map_data)
            db.session.add(current_map)
            db.session.commit()
            return make_response(jsonify({"status": "OK"}))
        except Exception as e:
            app.logger.error(e)
            return make_response(jsonify({"status": "notOK", "error": str(e)}))
    else:
        return render_template('map.html', current_map=current_map)
@app.route('/register', methods=["POST"])
def register():
    account = Account()
    account.name = request.form['uname']
    account.password = sha256_crypt.encrypt(str(request.form['pword']))
    account.dateOfBirth = request.form['dob']
    db.session.add(account)
    db.session.commit()
    return '<h1>Account Accepted</h1>'
@app.route('/login', methods=["POST"])
def login():
    loginData = Account.query.filter_by(name=request.form['uname']).first()
    if sha256_crypt.verify(loginData.password, request.form['pword']):
        #Create session and show account information
        session["log"] = True
        return render_template('account.html', data=loginData)
    else:
        return '<h1>Account information incorrect</h1>'

@app.route('/account')
def account():
    return render_template('account.html')


@app.route('/races')
def race():
    return render_template('race.html')

@app.route('/races/<string:race>')
def races(race):
    if race == 'dragonborn':
        return render_template('dragonborn.html')
    elif race == 'dwarf':
        return render_template('dwarf.html')
    elif race == 'elf':
        return render_template('elf.html')
    elif race == 'gnome':
        return render_template('gnome.html')
    elif race == 'halfelf':
        return render_template('halfelf.html')
    elif race == 'halfling':
        return render_template('halfling.html')
    elif race == 'halforc':
        return render_template('halforc.html')
    elif race == 'human':
        return render_template('human.html')
    elif race == 'tiefling':
        return render_template('tiefling.html')
    else:
        return render_template('race.html')

@app.route('/main/<int:session_id>')
def main_page(session_id):
    sessionData = Session.query.filter_by(id=session_id).first()
    return render_template('main.html',session=sessionData)
