from base64 import b64encode
from os import urandom, getenv

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO, emit

app = Flask('opendnd')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['SQLALCHEMY_DATABASE_URI'] = getenv('DBURI') or 'sqlite:///:memory:'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.secret_key = b64encode(urandom(256))

api = Api(app)
db = SQLAlchemy(app)
socketio = SocketIO(app)

from .models import *
from . import views
from . import rest


@app.before_first_request
def initialize():
    db.create_all()
    if Sheet.query.get(1):
        return

    sheet = Sheet()
    sheet.name = "Character"
    sheet.class_ = "Bard"
    sheet.level = 1
    sheet.background = "Acolyte"
    sheet.race = "Dwarf"
    sheet.username = "sagefhopkins"
    sheet.alignment = "Chaotic Good"
    sheet.experience = "100/1000"
    sheet.strength = "8"
    sheet.dexterity = "12"
    sheet.constitution = "11"
    sheet.intelligence = "9"
    sheet.wisdom = "12"
    sheet.charisma = "14"
    sheet.inspiration = "+2"
    sheet.proficiency_bonus = "+2"
    sheet.strength_save = "-1"
    sheet.dexterity_save = "+1"
    sheet.constitution_save = "+0"
    sheet.intelligence_save = "-1"
    sheet.wisdom_save = "+1"
    sheet.charisma_save = "+2"
    sheet.armor_class = "2"
    sheet.initiative = "1"
    sheet.speed = "35"
    sheet.current_hp = "25/35"
    sheet.temp_hp = "3"
    sheet.hit_dice = "1d6+3"
    sheet.succeeded_death_saves = "2/3"
    sheet.failed_death_saves = "1/3"
    gear = Equipment(content="Adventuring Gear")
    shield = Equipment(content="Sheild")
    backpack = Equipment(content="Backpack")
    potion = Equipment(content="Basic Poison")
    sheet.equipment.append(gear)
    sheet.equipment.append(shield)
    sheet.equipment.append(backpack)
    sheet.equipment.append(potion)
    sheet.gp = 20
    sheet.acrobatics = "+1"
    sheet.animal_handling = "+1"
    sheet.arcana = "-1"
    sheet.athletics = "-1"
    sheet.deception = "+2"
    sheet.history = "-1"
    sheet.insight = "+1"
    sheet.intimidation = "+2"
    sheet.investigation = "-1"
    sheet.medicine = "+1"
    sheet.nature = "-1"
    sheet.perception = "+1"
    sheet.performance = "+2"
    sheet.persuasion = "+2"
    sheet.religion = "-1"
    sheet.slight_of_hand = "+1"
    sheet.stealth = "+1"
    sheet.survival = "+1"
    staff = Attacks(item="Staff", attack="+5", damage="1d6+1")
    dagger = Attacks(item="Dager", attack="+9", damage="1d8+7")
    battle_axe = Attacks(item="Battleaxe", attack="+12", damage="2d4+4")
    sheet.attacks.append(staff)
    sheet.attacks.append(dagger)
    sheet.attacks.append(battle_axe)
    all_weapons = OtherProficiencies(content="All Weapons")
    all_armor = OtherProficiencies(content="All Armor")
    all_shields = OtherProficiencies(content="Shields")
    common = OtherProficiencies(content="Common")
    sylvan = OtherProficiencies(content="Sylvan")
    sheet.other_proficiencies.append(all_weapons)
    sheet.other_proficiencies.append(all_armor)
    sheet.other_proficiencies.append(all_shields)
    sheet.other_proficiencies.append(common)
    sheet.other_proficiencies.append(sylvan)
    reader = Personality(content="I've Read every book in Candlekeep")
    spender = Personality(content="Implusive Spender")
    sheet.personality.append(reader)
    sheet.personality.append(spender)
    sheet.ideals = "Believes those that take what they want will succeed, however isn't above helping others when it benefits himself"
    sheet.bonds = "Connected to all animals"
    sheet.flaws = "Values the lives of animals over even his closest friends"
    dark_vision = Feature(
        content=
        "Dark Vision: Accustomed to life Underground, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright, and in Darkness as if it were dim light. You can't discern color in Darkness, only shades of gray."
    )
    dwarven_combat_training = Feature(
        content=
        "Dwarven Combat Training: You have proficiency with the Battleaxe, Handaxe, Light Hammar, and Warhammar."
    )
    tool_proficiency = Feature(
        content=
        "Tool Proficiency: You gain proficiency with the artisan's tools of your choice: smith's tools, brewer's supplies, or mason's tools"
    )
    sheet.features.append(dark_vision)
    sheet.features.append(dwarven_combat_training)
    sheet.features.append(tool_proficiency)
    db.session.add(sheet)
    db.session.commit()
