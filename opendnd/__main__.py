#!/usr/bin/env python3

from os import getenv

import click

from . import socketio, app


@click.group()
@click.option('--debug/--no-debug', default=False, help='Toggle debugging.')
@click.pass_context
def main(ctx, debug):
    ctx.obj = {'debug': debug}


@main.command()
@click.option('-h', '--host', default='0.0.0.0', help='Address to listen on.')
@click.option('-p', '--port', default=5000, help='Port to listen on.')
@click.pass_obj
def run(config, host, port):
    """Run the development server."""
    click.echo(f'running the development server on {host}:{port}...')
    socketio.run(app, host=host, port=port, debug=config['debug'])


if __name__ == '__main__':
    main()
