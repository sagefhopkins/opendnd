#!/usr/bin/env python3

from pprint import PrettyPrinter

from flask import request
from flask_restful import Resource, marshal_with, fields, abort

from . import app, api, db
from .models import Sheet

pp = PrettyPrinter(indent=2).pprint


class FieldAdapter(dict):
    """Maps SQLAlchemy object to flask_resftul field dictionary"""
    def __init__(self, model):
        # Only get type from column object
        resource_fields = {
            name: column.type
            for name, column in model.__table__.columns.items()
        }

        # Adapt any single columns
        for name, column in resource_fields.items():
            if isinstance(column, db.String) or isinstance(column, db.Text):
                resource_fields[name] = fields.String
            elif isinstance(column, db.Integer):
                resource_fields[name] = fields.Integer

        # Adapt relationships
        # NOTE: this only works for many to many relationships
        # This was a giant PITA, please sqlalchemy implement JSON serialization
        for name, table in model.__mapper__.relationships.items():
            subfields = {
                name: column.type
                for name, column in table.target.columns.items()
            }

            for subname, column in subfields.items():
                if isinstance(column, db.String):
                    subfields[subname] = fields.String
                elif isinstance(column, db.Text):
                    subfields[subname] = fields.String
                elif isinstance(column, db.Integer):
                    subfields[subname] = fields.Integer

            resource_fields[name] = fields.List(fields.Nested(subfields))

        self.update(resource_fields)


class Character(Resource):
    url = '/api/character/<int:sheet_id>'
    fields = FieldAdapter(Sheet)

    @marshal_with(fields)
    def get(self, sheet_id, **kwargs):
        return Sheet.query.get_or_404(sheet_id)

    @marshal_with(fields)
    def put(self, sheet_id, **kwargs):
        sheet = Sheet.query.get_or_404(sheet_id)
        try:
            json = request.get_json()
            pp(json)
            sheet.merge_json(json)
            db.session.add(sheet)
            db.session.commit()
        except Exception as e:
            app.logger.error('bad request: %s', e)
            abort(400)

        return sheet


api.add_resource(Character, Character.url, endpoint=Character.url)
