OpenDND
=======

An opensource project designed to provide an alterative to D&D Beyond and Roll20
for players how would prefer to host their own D&D sessions without having to
pay fees for a full featured application
 
OpenDND Minimum Viable Product
------------------------------

* Character Sheets (**Complete**)
* VOIP
* Editable Maps (*Inprogress*)
* Campaign Management (Instance Management)
* Resource Pages (*Inprogress*)
* User Notepads
* Homepage (*Inprogress*)


OpenDND Future Development
--------------------------

* Dice Rolling
* Video Conferencing
* Mood Music
* Maps -> Rooms
* Maps -> Monster Information when hovering over object on canvas

Quick Start
-----------

::

    pip install -r requirements.txt
    pip install -e .
    opendnd run

